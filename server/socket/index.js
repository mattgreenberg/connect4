const Lobby = require('./Class/Lobby')

/**
 * Send the socket to the Lobby
 */
module.exports = (io) => {
  io.on('connection', (socket) => {
    Lobby.connectClient(socket)
  })
}
