const uuid = require('uuid/v1')
const logger = require('../../util/logger')

/**
 * The GameCore Class
 * A representation of one game on the server
 */
class GameCore {

  /**
   * Constuctor sets up game defaults
   */
  constructor() {
    this.hasRoom = true
    this.id = uuid()
    this.playerCount = 0
    this.clients = {}
    this.clientIds = []
    this.memory = new Array(63).fill(0)
    this.turn = 1
  }

  /**
   * Adds a socket (client) to the game
   * If two players are connected, we can start the game
   * @param {object} client [A socket object]
   */
  addPlayer(client) {
    this.playerCount += 1
    client.playerNumber = this.playerCount
    this.clientIds.push(client.id)
    this.clients[client.id] = client
    if (this.playerCount === 2) {
      this.hasRoom = false
      this.startGame()
    }
  }

  /**
   * Remove a socket (client) from the game
   * @param  {object}   client   [A socket object]
   * @param  {function} callback [A callback that runs when a player is disconnected]
   */
  removePlayer(client, callback) {
    this.clientIds.splice(this.clientIds.indexOf(client.id), 1)
    delete this.clients[client.id]
    callback(!this.clientIds.length)
  }

  /**
   * Starts the game
   */
  startGame() {
    this.bindSocketEvents()
    this.clientIds.forEach((id) => {
      this.clients[id].emit('start', {
        mem: this.memory,
        you: this.clients[id].playerNumber,
        turn: this.turn,
      })
    })
  }

  /**
   * Adds evented listeners to all of the clients
   */
  bindSocketEvents() {
    this.clientIds.forEach((id) => {
      const client = this.clients[id]
      client.on('move', this.onMove.bind(this, id))
      client.on('name', this.onName.bind(this, id))
    })
  }

  /**
   * Event callback for 'name'
   * Stores the player's inputed 'name' on the socket object
   * @param  {string} id   [The id of the socket that called the event]
   * @param  {string} name [The players name]
   */
  onName(id, name) {
    this.clients[id].name = name
    logger.log(`Client ${id} has selected name '${name}'`)
  }

  /**
   * Event callback for 'move'
   * Evalutes the players move and updates the game state.
   * Then notifys players of new state
   * @param  {string} id    [The id of the socket that called the event]
   * @param  {number} index [The move the user requested]
   */
  onMove(id, index) {
    const client = this.clients[id]
    // verify its your turn first
    if (client.playerNumber === this.turn) {
      // validate the spot is open
      if (this.memory[index] === 0) {
        this.memory[index] = this.turn
        const winColor = this.turn === 1 ? 'blue' : 'red'
        this.switchTurns()
        logger.log(`acknowledged move '${index}' from [${id}]`)
        const win = this.checkForWin(index)
        const enemyId = this.clientIds.indexOf(id) === 0 ? this.clientIds[1] : this.clientIds[0]
        this.clients[enemyId].emit('attack', {
          piece: {
            index,
            owner: this.memory[index],
          },
          mem: this.memory,
          turn: this.turn,
          win: (win) ? { win, name: client.name, color: winColor } : false,
        })
        client.emit('ack', {
          index,
          mem: this.memory,
          turn: this.turn,
          win: (win) ? { win, name: client.name, color: winColor } : false,
        })
      }
    } else {
      logger.log('Invalid Request', id, index)
    }
  }

  /**
   * Simple function to switch player's turns
   */
  switchTurns() {
    this.turn = this.turn === 1 ? 2 : 1
  }

  /**
   * Evalutes if the last move played produced a win
   * @param  {number} pieceIndex [Index of the last move played in memory]
   * @return {array}             [The winning lines]
   */
  checkForWin(pieceIndex) {
    // we scope function 'r' to protect the parameters of recursion
    const winningSets = []
    const r = (index, increment = 1, arr = [index], done = false) => {

      if (this.memory[index] === this.memory[index + increment]) {
        arr.push(index + increment)
        return r(index + increment, increment, arr, done)
      } else if (done) {
        if (arr.length >= 4) {
          // handle the win here
          winningSets.push(arr)
        }
        const abs = Math.abs(increment)
        if (abs === 1) {
          // continue with next direction check
          return r(arr[0], 8)
        } else if (abs < 10) {
          // continue with next direction check
          return r(arr[0], abs + 1)
        }
      } else {
        return r(arr[0], -increment, arr, true)
      }
      // the recursion is over, return winning sets if there is any
      return (winningSets.length > 0) ? winningSets : false
    }
    // Kick it off here
    return r(pieceIndex)
  }

}

// export our GameCore class
module.exports = GameCore
