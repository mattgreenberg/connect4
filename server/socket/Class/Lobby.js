const GameCore = require('./GameCore')
const logger = require('../../util/logger')

/**
 * The Lobby class keeps track of the sockets and game instances
 */
class Lobby {

  /**
   * Setup Lobby defaults
   */
  constructor() {
    this.games = {}
    this.gameIds = []
  }

  /**
   * Add a socket 'client' to the Lobby
   * @param  {object} client [a socket object]
   */
  connectClient(client) {
    logger.log(`New client connected : ${client.id}`)
    this.findGame(client)
    client.on('disconnect', () => {
      logger.log(`Client disconnected : ${client.id}`)
      this.dropPlayer(client)
    })
  }

  /**
   * Find a game for a socket 'client'
   * if no game is available, make one
   * @param  {object} client [a socket object]
   */
  findGame(client) {
    // look for an open game
    for (let i = 0; i < this.gameIds.length; i += 1) {
      const game = this.games[this.gameIds[i]]
      if (game.hasRoom) {
        client.gameId = game.id
        game.addPlayer(client)
        return
      }
    }
    // no game found, start a new one
    const game = new GameCore()
    client.gameId = game.id
    game.addPlayer(client)
    logger.log(`New game started: ${client.gameId}`)
    this.gameIds.push(game.id)
    this.games[game.id] = game
  }

  /**
   * Remove player from the Lobby
   * @param  {object} client [a socket object]
   */
  dropPlayer(client) {
    const game = this.games[client.gameId]
    game.removePlayer(client, (gameIsOver) => {
      if (gameIsOver) {
        delete this.games[client.gameId]
        this.gameIds.splice(this.gameIds.indexOf(client.gameId), 1)
        logger.log(`Inactive game closed: ${client.gameId}`)
      }
    })
  }
}

// export a new Lobby object
module.exports = (() => new Lobby())()
