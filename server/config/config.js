/* eslint global-require: 0, import/no-dynamic-require: 0 */
const merge = require('deepmerge')

// setup the base configuration
const config = {
  dev: 'development',
  prod: 'production',
  port: process.env.PORT || 3000,
}

// set default node enviornment if not already set
process.env.NODE_ENV = process.env.NODE_ENV || config.dev

// add enviornment to config object
config.env = process.env.NODE_ENV

// extend config based on enviornment
let envConfig
try {
  envConfig = require(`./${config.env}`)
  envConfig = envConfig || {}
} catch (e) {
  envConfig = {}
}

// merge : properties in envConfig take precedence
module.exports = merge(config, envConfig)
