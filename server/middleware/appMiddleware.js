const morgan = require('morgan')
const express = require('express')

// export a function that adds middle ware to our application
module.exports = (app) => {
  app.use(morgan('dev'))
  app.use(express.static('public'))
}
