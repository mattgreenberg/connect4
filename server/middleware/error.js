/* eslint no-unused-vars: 0 */
const logger = require('../util/logger')

// create an error handling middleware
module.exports = () =>
  (err, req, res, next) => {

    if (err) {
      logger.log(err)
      res.status(500).send('An unexpected error occured.')
    }

  }
