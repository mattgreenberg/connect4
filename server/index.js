const app = require('express')()
const server = require('http').Server(app)
const io = require('socket.io')(server, {
  serveClient: false,
  pingInterval: 10000,
  pingTimout: 5000,
})
const middleware = require('./middleware/appMiddleware')
const err = require('./middleware/error')
const socket = require('./socket')

// add our middleware
middleware(app)

// mount error handling
app.use(err())

// add our socket event handlers
socket(io)

// expose our app
module.exports = server
