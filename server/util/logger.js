// require some dependencies
const config = require('../config/config')
const chalk = require('chalk')

// create a no operation function for when logging is disabled
const noop = () => {}

// check if its an error
const isError = e => e && e.stack && e.message

// check if logging is enabled, if it is, use console.log, else use noop
const consoleLog = config.logging ? console.log : noop

// make a simple logging function that stringifys stuff
const logger = {
  log: (...args) => {

    const fancyArgs = args.map((arg) => {

      if (isError(arg)) {

        let errorData = `${chalk.red('Error: ')} ${chalk.blue(arg.message)} \n`
        errorData += `${chalk.red('Stack: ')} ${chalk.yellow(arg.stack)}`
        return errorData

      } else if (Array.isArray(arg)) {

        return chalk.dim(JSON.stringify(arg))

      } else if (typeof arg === 'object') {

        return chalk.magenta(JSON.stringify(arg, null, 2))

      }

      return chalk.blue(arg.toString())

    })

    consoleLog.apply(console, fancyArgs)

  },
}

module.exports = logger