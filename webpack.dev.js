const merge = require('webpack-merge')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  devServer: {
    contentBase: './public',   // public directory
    publicPath: '/',           // path from public directory to serve bundles 
    port: 3000,
    inline: true,
  },
  devtool: 'inline-source-map',
})
