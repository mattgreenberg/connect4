const config = require('./server/config/config')
const app = require('./server')
const logger = require('./server/util/logger')

// Start the server
app.listen(config.port, () => {
  logger.log(`Server listening on port: ${config.port}`)
})
