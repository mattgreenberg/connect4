#### Day 1

**Setting Up My Development Environment**

For the front end, I am going to use HTML5 (canvas), CSS3 (SASS), and ES6. I was going to use Gulp.js as my task runner, but switched to Webpack for asset bundling. I will use Babel to transpile es6 into native js. I will also use the webpack flavored SASS loader to compile my SASS into CSS. All of my front end assets will be bundled. I am going to use a flavor of the AirBNB ESlint config to lint my js. I think I am ready to start development now.


**Getting a the game board design setup built**

I know that I am going to use the `<canvas>` element to render the game. I actually plan on using two `<canvas>` elements stacked on top of each other. The one on top will handle the display of the board structure, the one on bottom will render the checkers. This is a common design pattern for html5 game development. I need to figure out how I am going to pain the game board first.

#### Day 2

**Working on the checkers**

I have the basic idea for the board flushed out and working well. I need to start working on the checkers now. I plan on using `window.requestAnimationFrame` for the animation callbacks. I need to think about how I am going to store the state of the game in memory. Here is a solution I came up with.

![Game In Memory](./boardlayoutinmemory.png)

While this type of memory solution will work for validating game play, i still need another data store that keeps track of the checker pieces that need to be pained for the UI. Working on that solution now. I have the basic idea mapped out, but I want to add some better animations, and maybe some sounds.

#### Day 3

**Finishing the game Interface**

I was able to get the animations working properly and saved in memory. I can now drop checkers into place. All of the basic animations are working and you hear a 'click' sound when the checker hits the bottom of the column it was dropped into.

#### Day 4

**Finding the winning line**

This is probably the most important part of the project. I spent some time optimizing the solution I had originally thought of. I am happy with the solution I came up with.

#### Day 5

Did a little more optimizing of `checkForWin` function. I need to move on to making the game multiplayer now that I have all the mechanics worked out. In preparation for working on the server side code, I split my webpack configs into development and production. I just don't need the dev server in production bundle.

#### Day 6

**Server Setup and Development**

Setup my server defaults, now I can start working on the socket communication. Going to start with a `lobby` which connects players into a game.

I now have the basic Lobby setup and will begin working on the server GameCore which will handle the logic on the server. I am running out of time, so I am going to have to cut out some 'features' I originally intended for.

#### Day 7

**Crunch Time**

Lets see how far I get, I don't want to ask for more time. I was able to get the multiplayer events working. I just need the server to look for 'wins', then I have to clean up the code.

All the mechanics of the game are done, I just have to clean up the user interface. The interface is as good as I could have made it within the given time. I was able to add some much needed comments.

#### Reflection

It is now 7:30 pm on Thursday Night. I enjoyed the project development and might continue to refine it in my own time. I know there are a few things I wish I had more time to complete.

1. A more capable and flexible interface
2. Integrating game chat (which is usually easy with sockets)
3. Lobby control (letting users create their own games instead of the server just connecting two players)
3. Handle mid game disconnects (more of a missing feature)
5. Some chai 'expect' flavored tests

Thank you for taking the time to read my notes.

-Matt
