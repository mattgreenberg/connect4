### **Connect Four**

A web based rendition of the classic game, [Connect Four](https://en.wikipedia.org/wiki/Connect_Four).

___

#### _To The Focus on the Family Development Team_

It is my intention to complete the project to the best of my ability. In an effort to help you better understand my thought process, I have included a `journal` with some notes I took along the way. [You can find that information here!](./JOURNAL.md)

I hope you enjoy my project!

___

#### Documentation

**Getting Started**

You are going to need Node.js (version 7.5.0 or greater due to new Object methods) to get this project working. The best way to get the project onto your machine is to clone the repository. After cloning switch to the root directory of the project and run `npm install` from your terminal to install the required dependencies.

``` bash
# Install Required Dependencies (do this first!)
$: npm install

# Build the production bundle
$: npm run build

# Launch the server
$: node index
```

**Working on Front End Development**

``` bash
# Start Development Server
$: npm start

# Build Webpack Bundles for Production
$: npm run build
```

**Workin on Back End Development**

``` bash
# Start server in development mode
$: npm run dev
```

___

**Developed By:**<br>
Matthew Greenberg<br>
magreenberg@fullsail.edu<br>
719.339.4872<br>
