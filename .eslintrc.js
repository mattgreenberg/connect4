module.exports = {
  "env": {
    "browser": true,
    "node": true,
    
  },
  "extends": "airbnb-base",
  "rules": {

    // allow console.logs
    "no-console": 0,

    // no semi colons
    "semi": ["error", "never"],

    // single quotes only
    "quotes": ["error", "single"],

    // allow padded blocks
    "padded-blocks": 0,

    // really annoying when working with sockets
    "no-param-reassign": 0,

  },
};