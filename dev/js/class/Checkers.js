import canvas from '../util/canvas'
import color from '../util/colors'
import click from '../../sound/click.mp3'

/**
 * Checkers Canvas Class
 * Handles the animation of the checkers
 * and the main game state on the client
 */
class Checkers {

  /**
   * Set up all the default values
   * @param  {object} client [The client socket object]
   * @param  {object} styles [Key value pairs of canvas element]
   * @param  {object} size   [Game Size data]
   * @param  {object} info   [The DOM element used to display game information]
   */
  constructor(client, styles, size, info) {
    this.info = info
    this.client = client
    this.gameSize = size
    this.gravity = 900
    this.click = new Audio(click)
    this.animateDisplay('Looking for an opponent')

    this.canvas = canvas('checkers', this.gameSize.square, this.gameSize.square)
    this.ctx = this.canvas.getContext('2d')
    this.ctx.lineWidth = 5
    this.box = this.gameSize.square / this.gameSize.split
    this.mouseX = this.gameSize.square / 2

    // interate over style object and add the styles to our canvas element
    Object.entries(styles).forEach(([key, value]) => {
      this.canvas.style[key] = value
    })

    // initalize our socket
    this.initSocket()
  }

  /**
   * Add a simple display message with a 'waiting' animation
   * @param  {string} base [The string to display]
   */
  animateDisplay(base) {
    const len = base.length
    this.display(base)
    this.displayAnimationId = setInterval(() => {
      if (this.info.innerHTML.length < len + 3) {
        this.info.innerHTML += '.'
      } else {
        this.display(base)
      }
    }, 300)
  }

  /**
   * Add a simple display message
   * @param  {string} info [The string to display]
   */
  display(info) {
    this.info.innerHTML = info
  }

  /**
   * Test to see if it is this client's turn
   * @return {boolean} [true if its your turn]
   */
  myTurn() {
    return this.me === this.turn
  }

  /**
   * Bind event listeneres for the socket
   */
  initSocket() {
    const socket = this.client
    socket.on('start', this.onStart.bind(this))
    socket.on('ack', this.onAck.bind(this))
    socket.on('attack', this.onAttack.bind(this))
  }

  /**
   * Socket event that marks the start of the game
   * @param  {object} data [The server game information]
   */
  onStart(data) {
    this.client.emit('name', this.client.name)
    this.me = data.you
    this.turn = data.turn
    this.memory = data.mem
    this.pieces = []
    this.win = false
    this.canvas.addEventListener('mousemove', this.onMouseMove.bind(this))
    this.canvas.addEventListener('click', this.onMouseClick.bind(this))
    clearTimeout(this.displayAnimationId)
    if (this.myTurn()) {
      this.display('Place your checker!')
      this.activePiece = this.newActivePiece()
    } else {
      this.animateDisplay('Waiting On Blue To Move')
    }
    // start the animation event loop
    this.update(new Date().getTime())
  }

  /**
   * Socket event that fires when the server acknowledges your last valid move
   * @param  {object} data [The server game information]
   */
  onAck(data) {
    // update memory
    this.memory = data.mem
    this.turn = data.turn
    this.win = data.win
    const { xPos, yPos } = this.translateCordFrom(data.index)
    this.activePiece.x = xPos
    this.activePiece.destY = yPos
    this.activePiece.state = 'falling'
  }

  /**
   * Socket event that fires when the server acknowledges an enemies move
   * @param  {object} data [The server game information]
   */
  onAttack(data) {
    clearTimeout(this.displayAnimationId)
    this.memory = data.mem
    this.turn = data.turn
    this.win = data.win
    this.activePiece = this.newActivePiece()
    const { xPos, yPos } = this.translateCordFrom(data.piece.index)
    this.activePiece.owner = data.piece.owner
    this.activePiece.x = xPos
    this.activePiece.destY = yPos
    this.activePiece.state = 'falling'
  }

  /**
   * Gives the x and y position of the board by evaluting the index of the position in memory
   * @param  {number} index [The index of the piece in memory]
   * @return {object}       [An object with xPos & yPos]
   */
  translateCordFrom(index) {
    const xPos = ((index % 9) * this.box) - (this.box / 2)
    const yPos = this.gameSize.square - ((Math.floor(index / 9) * this.box) + (this.box / 2))
    return { xPos, yPos }
  }

  /**
   * Build a new active piece
   * @return {object} [A new active piece]
   */
  newActivePiece() {
    const newObj = Object.assign({}, {
      x: this.gameSize.square / 2,
      y: this.box / 2,
      owner: this.turn,
      destY: this.gameSize.square,
      state: 'selecting',
    })
    return newObj
  }

  /**
   * The update event loop which calculates delta time for animations
   * @param  {DateTime} t [The current time the event is fired]
   */
  update(t) {
    this.dt = this.lastframetime ? ((t - this.lastframetime) / 1000.0).toFixed(2) : 0.016
    this.lastframetime = t
    this.tick()
    if (this.stop) {
      console.log('stopping')
      return
    }
    this.frameId = window.requestAnimationFrame(this.update.bind(this))
  }

  /**
   * A function that updates and paints all the things
   */
  tick() {
    this.updateActivePieceLocation()
    this.ctx.clearRect(0, 0, this.gameSize.square, this.gameSize.square)
    if (this.activePiece) {
      this.drawPiece(this.activePiece)
    }
    this.pieces.forEach((piece) => { this.drawPiece(piece) })
    if (this.winningPieces) {
      this.ctx.fillStyle = color.green
      this.ctx.strokeStyle = color.greenBorder
      this.winningPieces.forEach((set) => {
        set.forEach((index) => {
          const { xPos, yPos } = this.translateCordFrom(index)
          this.ctx.beginPath()
          this.ctx.arc(xPos, yPos, 37, 0, Math.PI * 2, true)
          this.ctx.fill()
          this.ctx.stroke()
        })
      })
      this.stop = true
      this.info.innerHTML += '<br/><a class="startover" href="/">Play Again</a>'
      this.client.disconnect()
    }
  }

  /**
   * Draws a piece on the board
   * @param  {object} piece [The piece to be drawn]
   */
  drawPiece(piece) {
    this.ctx.fillStyle = (piece.owner === 1) ? color.blue : color.red
    this.ctx.strokeStyle = (piece.owner === 1) ? color.blueBorder : color.redBorder
    this.ctx.beginPath()
    this.ctx.arc(piece.x, piece.y, 37, 0, Math.PI * 2, true)
    this.ctx.fill()
    this.ctx.stroke()
  }

  /**
   * Updates the state and position of the active piece
   */
  updateActivePieceLocation() {
    // if there is no active piece, do nothing
    if (!this.activePiece) return
    // update active piece based on state
    if (this.activePiece.state === 'selecting') {
      this.activePiece.x = this.mouseX
    } else if (this.activePiece.state === 'falling') {
      this.activePiece.y += (this.gravity * this.dt)
      if (this.activePiece.y >= this.activePiece.destY) {
        this.activePiece.y = this.activePiece.destY
        this.activePiece.state = 'placed'
        this.click.cloneNode(true).play()
        setTimeout(this.readyNextPlayer.bind(this), 300)
      }
    }
  }

  /**
   * Update the game interface with the next move or win
   */
  readyNextPlayer() {
    this.pieces.push(this.activePiece)
    this.activePiece = null
    if (this.win) {
      this.display(`Looks Like '${this.win.name}' (${this.win.color}) Won!`)
      this.winningPieces = this.win.win
      return
    }
    if (this.myTurn()) {
      this.display('Place Your Checker!')
      this.activePiece = this.newActivePiece()
    } else {
      this.animateDisplay(`Waiting On ${this.turn === 1 ? 'Blue' : 'Red'} To Move`)
    }
  }

  /**
   * Look for an open move on a column
   * If move is available, take it and notify the server
   * @param  {number} column [the number of the column]
   */
  attemptMove(column) {
    // loop through rows and look for a valid move on that column
    for (let i = 0; i < (this.gameSize.split - 1); i += 1) {
      const index = column + (i * 9)
      // If the move is open, request it
      if (this.memory[index] === 0) {
        this.client.emit('move', index)
        break
      }
    }
  }

  /**
   * mousemove Event that keeps track of the x position of the mouse
   * @param  {object} event [the mousemove event object]
   */
  onMouseMove(event) {
    const rect = this.canvas.getBoundingClientRect()
    this.mouseX = ((event.clientX - rect.left) / (rect.right - rect.left)) * this.gameSize.square
    this.mouseX = Math.round(this.mouseX)
    const halfBox = this.box / 2
    if (this.mouseX < halfBox) {
      this.mouseX = halfBox
    } else if (this.mouseX > (this.gameSize.square - halfBox)) {
      this.mouseX = (this.gameSize.square - halfBox)
    }
  }

  /**
   * click Event that fires on mouse click
   * @param  {object} event [the click event object]
   */
  onMouseClick(event) {
    if (!this.activePiece || this.activePiece.state !== 'selecting') return
    const rect = this.canvas.getBoundingClientRect()
    let posX = ((event.clientX - rect.left) / (rect.right - rect.left)) * this.gameSize.square
    posX = Math.round(posX)
    for (let i = 0; i < this.gameSize.split; i += 1) {
      const bound = i * this.box
      if (bound <= posX && posX < (bound + this.box)) {
        this.attemptMove(i + 1)
        break
      }
    }
  }

}

// export the Checkers class
export default Checkers
