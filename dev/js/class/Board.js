import canvas from '../util/canvas'
import color from '../util/colors'

/**
 * The Board class just paints the board
 */
class Board {

  /**
   * Setup default values and init
   * @param  {object} styles [KvP of canvas css styles in js form]
   * @param  {object} size   [The game size data]
   */
  constructor(styles, size) {
    this.gameSize = size
    this.canvas = canvas('board', this.gameSize.square, this.gameSize.square)
    this.ctx = this.canvas.getContext('2d')
    Object.entries(styles).forEach(([key, value]) => {
      this.canvas.style[key] = value
    })
    this.paint()
  }

  /**
   * Paints the game board
   */
  paint() {
    const block = this.gameSize.square / this.gameSize.split
    this.ctx.fillStyle = color.darkBlue
    this.ctx.strokeStyle = color.darkBlueBorder

    // fill the board rectangle & give it an outline
    this.ctx.lineWidth = 6
    this.ctx.fillRect(0, block, this.gameSize.square, this.gameSize.square - block)
    this.ctx.strokeRect(3, block + 3, this.gameSize.square - 6, (this.gameSize.square - 6) + block)
    this.ctx.lineWidth = 5

    // draw our dividing lines
    for (let i = 1; i < this.gameSize.split; i += 1) {
      this.ctx.beginPath()
      this.ctx.moveTo(i * block, block)
      this.ctx.lineTo(i * block, this.gameSize.square)
      this.ctx.stroke()
      this.ctx.beginPath()
      this.ctx.moveTo(0, (i * block) + block)
      this.ctx.lineTo(this.gameSize.square, (i * block) + block)
      this.ctx.stroke()
    }

    // cut circles by using a clipping mask
    const radius = parseInt(block * 0.35, 10)
    this.ctx.beginPath()
    for (let row = 1; row < this.gameSize.split; row += 1) {
      const y = (row * block) + (block / 2)
      for (let column = 0; column < this.gameSize.split; column += 1) {
        const x = (column * 100) + (block / 2)

        this.ctx.moveTo(x, y)
        this.ctx.arc(x, y, radius, 0, Math.PI * 2, true)

      }
    }
    this.ctx.clip()
    this.ctx.clearRect(0, block, this.gameSize.square, this.gameSize.square - block)
  }
}

// export our Board class
export default Board
