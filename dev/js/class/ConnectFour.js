import Board from './Board'
import Checkers from './Checkers'

/**
 * The Connect Four Game Class
 */
class ConnectFour {
  constructor(client, id, info) {
    this.root = document.getElementById(id)
    this.info = document.getElementById(info)
    this.gameSize = {
      square: 700,
      split: 7,
    }
    this.checkers = new Checkers(client, {
      position: 'absolute',
      zIndex: 0,
    }, this.gameSize, this.info)
    this.board = new Board({
      zIndex: 1,
      position: 'relative',
      pointerEvents: 'none',
    }, this.gameSize)

    this.mountGame()
  }

  mountGame() {
    const container = document.createElement('div')
    container.style.position = 'relative'
    container.appendChild(this.board.canvas)
    container.appendChild(this.checkers.canvas)
    this.root.appendChild(container)
  }

}

export default (client, id, info) => new ConnectFour(client, id, info)
