export default {
  white: '#ecf0f1',
  blue: '#3498db',
  blueBorder: '#2980b9',
  red: '#e74c3c',
  redBorder: '#c0392b',
  darkBlue: '#34495e',
  darkBlueBorder: '#2c3e50',
  green: '#2ecc71',
  greenBorder: '#27ae60',
}
