/**
 * A simple function that creates a 'canvas'
 * @param  {string} id     [The id of the canvas element]
 * @param  {number} width  [The width of the canvas element]
 * @param  {number} height [The height of the canvas element]
 * @return {object}        [A DOM canvas element]
 */
export default (id, width, height) => {
  const canvas = document.createElement('canvas')
  canvas.id = id
  canvas.width = width
  canvas.height = height
  canvas.style.top = 0
  canvas.style.left = 0
  canvas.style.width = '100%'
  return canvas
}
