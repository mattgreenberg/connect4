import io from 'socket.io-client'
import '../scss/main.scss'
import ConnectFour from './class/ConnectFour'

const errorMsg = document.querySelector('p.error')
const nameForm = document.getElementById('nameform')

const startGame = (name) => {
  nameForm.style.display = 'none'
  const socket = io()
  socket.name = name
  ConnectFour(socket, 'root', 'game_info')
}

document.querySelector('input[type=text]').addEventListener('input', (event) => {
  event.preventDefault()
  errorMsg.style.display = 'none'
})

document.querySelector('form').addEventListener('submit', (event) => {
  event.preventDefault()
  const playerName = event.target[0].value.trim()
  if (playerName === '') {
    errorMsg.style.display = 'block'
    event.target[0].focus()
    event.target[0].select()
  } else {
    startGame(playerName)
  }
  return false
})
